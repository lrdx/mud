#ifndef __MSDP_CONSTANTS_HPP_
#define __MSDP_CONSTANTS_HPP_

#include <string>

namespace msdp
{
	namespace constants
	{
		const std::string ROOM = "ROOM";
		const std::string EXPERIENCE = "EXPERIENCE";
		const std::string GOLD = "GOLD";
		const std::string LEVEL = "LEVEL";
		const std::string MAX_HIT = "MAX_HIT";
		const std::string MAX_MOVE = "MAX_MOVE";
		const std::string STATE = "STATE";
	}
}

#endif // __MSDP_CONSTANTS_HPP_